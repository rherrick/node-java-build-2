FROM ubuntu:xenial

ENV DEBIAN_FRONTEND noninteractive
ENV NODE_VERSION 8.15.0
ENV YARN_VERSION 1.12.3
ENV JAVA_HOME "/opt/java/linux-x64"
ENV PATH "${JAVA_HOME}/bin:${PATH}"

# node installations command expect to run as root
## Using node installation from https://raw.githubusercontent.com/nodejs/docker-node/3e539e6925a524bf4fda47ea33ed33d0d4fb0e20/10/stretch/Dockerfile
USER root

# Add 32-bit arch, add winehq key and repo, update, upgrade, and clean up, then install curl, winehq, and xz-utils
RUN apt-get -y update \
  && apt-get -y upgrade \
  && apt-get -y install apt-transport-https curl sudo unzip

RUN dpkg --add-architecture i386 \
  && curl -s https://dl.winehq.org/wine-builds/winehq.key | apt-key add \
  && echo "deb https://dl.winehq.org/wine-builds/ubuntu/ xenial main" > /etc/apt/sources.list.d/winehq.list \
  && apt-get -y update \
  && apt-get -y install curl g++ git libcurl4-openssl-dev make python python-pip winehq-stable xz-utils \
  && pip install --upgrade httpie

# Add node and circleci users
RUN groupadd --gid 1000 node \
  && useradd --uid 1000 --gid node --shell /bin/bash --create-home node \
  && groupadd --gid 3434 circleci \
  && useradd --uid 3434 --gid circleci --shell /bin/bash --create-home circleci \
  && usermod --append --groups sudo circleci \
  && echo "circleci ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10-circleci

RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
    FD3A5288F042B6850C66B31F09FE44734EB7990E \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
    DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 \
    77984A986EBC2AA786BC0F66B01FBB92821C587A \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
    6A010C5166006599AA17F08146C2130DFD2497F5 \
  ; do \
    gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
    gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
    gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /usr/local \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  && ln -s /usr/local/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
  && ln -s /usr/local/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz

# Download JREs for building app
#
# Using Zulu JRE builds to avoid Oracle licensing issues.
#
# Linux 64-bit:   https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-linux_x64.tar.gz
# Linux 32-bit:   https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-linux_i686.tar.gz
# Windows 64-bit: https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-win_x64.zip
# Windows 32-bit: https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-win_i686.zip
# OS X 64-bit:    https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-macosx_x64.zip
#
# Add this back to the command below if we need to provide Win32 support.
#
#  && curl -SLO https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-win_i686.zip \
#  && unzip -d /opt/java zulu8.33.0.1-jdk8.0.192-win_i686.zip \
#  && rm -f zulu8.33.0.1-jdk8.0.192-win_i686.zip \
#  && mv /opt/java/zulu8.33.0.1-jdk8.0.192-win_i686 /opt/java/win-ia32 \

RUN mkdir -p /opt/java \
  && curl -qSLO https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-win_x64.zip \
  && curl -qSLO https://cdn.azul.com/zulu/bin/zulu8.33.0.1-jdk8.0.192-linux_x64.tar.gz \
  && unzip -qqd /opt/java zulu8.33.0.1-jdk8.0.192-win_x64.zip \
  && tar -xf zulu8.33.0.1-jdk8.0.192-linux_x64.tar.gz -C /opt/java \
  && rm -f zulu8.33.0.1-jdk8.0.192* \
  && mv /opt/java/zulu8.33.0.1-jdk8.0.192-win_x64 /opt/java/win-x64 \
  && mv /opt/java/zulu8.33.0.1-jdk8.0.192-linux_x64 /opt/java/linux-x64 \
  && chown -R circleci:circleci /opt/java

USER circleci
